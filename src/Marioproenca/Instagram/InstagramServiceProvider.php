<?php
/**
 * Marioproenca/Instagram service provider class
 * User: marioproenca
 * Date: 08/11/15
 * Time: 21:06
 */

namespace Marioproenca\Instagram;


use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

/**
 * Class InstagramServiceProvider
 *
 * @package Marioproenca\Instagram
 */
class InstagramServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Enable package autoloading
     */
    public function boot()
    {
        $this->package('marioproenca/instagram');

        AliasLoader::getInstance()->alias('Instagram', 'Marioproenca\Instagram\Instagram');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

}
