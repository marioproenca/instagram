<?php
/**
 * Configuration file
 * User: marioproenca
 * Date: 08/11/15
 * Time: 21:13
 */
return [
    /**
     * Client ID
     * Register at http://instagram.com/developer/ and replace client_id with your own
     */

    "client_id" => "",
    /**
     * Total images to get
     */
    "total_images" => 18,

    /**
     * Default image width
     */
    "image_width" => 120,

    /**
     * Default image height
     */
    "image_height" => 120,

    /**
     * Default tag
     */
    "tag" => "London"

];